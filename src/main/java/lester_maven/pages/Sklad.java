package lester_maven.pages;


import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.WebDriverWait;

/*0661864246 - Саша
0930220627 - Саша Лайф
0955789631 - Антон
0507125525 - Вика
0951702079 - Аня
0663033934 - Ира
0508246084 - Dasha
0937320128 - Sony
0937878742 - Ira Life
0973040297 - Дима Кучура
0994247092 - Аня Мартыненко
*/




public class Sklad{

    static WebDriver driver;
    WebDriverWait wait;
    public static String phone = "0994247092"; //Аня
    public String ProductPage = "http://skladshin.com/3649/legkovaja-shina-winter-155-70-r13-75-q";

    @BeforeClass
    public void setup(){

        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        //S wait = new WebDriverWait(driver, 60);
        //driver.navigate().to("http://bolidov.com.ua/");
        //driver.manage().deleteAllCookies();
    }

    @AfterClass
    public void teardown() throws InterruptedException {
        driver.quit();
    }

    @Test(priority =1)
    public void zakazZvonka() throws InterruptedException {

        driver.get("http://skladshin.com/");
        driver.findElement(By.id("newCallbackInit")).click();
        driver.findElement(By.className("hunterSubmit")).click();
        Thread.sleep(1500);
        driver.findElement(By.id("backPhone")).sendKeys(phone);
        driver.findElement(By.className("hunterSubmit")).click();

        Thread.sleep(2000);

        System.out.println("skladshin_zakazZvonka:done");



    }

    @Test(priority =2)
    public void bisrtiyZakaz() throws InterruptedException {

        driver.get(ProductPage);
        Thread.sleep(3000);
        driver.findElement(By.xpath("html/body/div[3]/div/div/div[2]/div[2]/div[1]/div[3]/div[1]/div[2]/p[2]")).click();
        driver.findElement(By.xpath(".//*[@id='o_click']/div/button")).click();
        driver.findElement(By.xpath(".//*[@id='zz']")).sendKeys("0000000004");
        driver.findElement(By.xpath(".//*[@id='o_click']/div/button")).click();

		/*	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Спасибо за заказ! Администратор ответит Вам в ближайшее время']")));


			if(driver.getPageSource().contains
					("Спасибо за заказ! Администратор ответит Вам в ближайшее время")) {
				 System.out.println("skladshin_bistriyZakaz:done");
			}
			else {
				System.out.println("skladshin_bistriyZakaz:fail");
			}*/
        System.out.println("skladshin_bistriyZakaz:done");
        Thread.sleep(3000);
    }

    @Test(priority =3)
    public void zakaz() throws InterruptedException {
        driver.get("http://skladshin.com/");

        driver.findElement(By.xpath(".//*[@data-href='#enterReg']")).click();
        driver.findElement(By.xpath(".//*[@type='email']")).sendKeys("zolotarevskaya.i.wezom@gmail.com");
        driver.findElement(By.xpath(".//*[@type='password']")).sendKeys("123456");
        driver.findElement(By.xpath(".//*[@id='entrForm']/div[4]/button")).click();
        driver.get(ProductPage);
//		 driver.findElement(By.xpath("html/body/div[3]/div/div/div[2]/div[2]/div[3]/div[1]/div[2]/p[1]")).click();
        driver.findElement(By.xpath("//*[@class='buy_normal addToCart']")).click();
        driver.findElement(By.xpath(".//*[@href='/cart/personal_data']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("html/body/div[3]/div/div/div[1]/div[3]/div[2]/div/div[2]/form/div[3]/button")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//*[@id='it']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//*[@name='delivery1']")).sendKeys("К. Маркса, 18 (напротив Укртелекома)");

        driver.findElement(By.xpath("html/body/div[3]/div/div/div[1]/div[3]/div[2]/div/div/form/div[4]/button")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id='pb']")).click();// Дописал
        Thread.sleep(2000);
        driver.findElement(By.xpath("html/body/div[3]/div/div/div[1]/div[3]/div[2]/form/div[2]/button")).click();
        Thread.sleep(3000);
        if(driver.getPageSource().contains("Заказ успешно оформлен")) {
            System.out.println("skladshin_zakaz:done");
        }
        else {
            System.out.println("skladshin_zakaz:fail");
        }

    }


}
